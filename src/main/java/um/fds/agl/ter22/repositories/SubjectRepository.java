package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;
import um.fds.agl.ter22.entities.SubjectTER;


public interface SubjectRepository extends SubjectBaseRepository<SubjectTER> {
    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (#subject?.userInCharge?.lastName == authentication?.name)")
    SubjectTER save(@Param("subject") SubjectTER subject);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (@subjectRepository.findById(#id).get()?.userInCharge?.lastName == authentication?.name)")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or (#subject?.userInCharge?.lastName == authentication?.name)")
    void delete(@Param("subject") SubjectTER subject);
}
