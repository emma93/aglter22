package um.fds.agl.ter22.repositories;

import org.springframework.data.repository.query.Param;
import org.springframework.security.access.prepost.PreAuthorize;

import um.fds.agl.ter22.entities.GroupTER;


public interface GroupRepository extends GroupBaseRepository<GroupTER> {
    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_STUDENT')")
    GroupTER save(@Param("group") GroupTER group);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void deleteById(@Param("id") Long id);

    @Override
    @PreAuthorize("hasRole('ROLE_MANAGER')")
    void delete(@Param("group") GroupTER group);
}
