package um.fds.agl.ter22.controllers;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import um.fds.agl.ter22.entities.SubjectTER;
import um.fds.agl.ter22.entities.Teacher;
import um.fds.agl.ter22.forms.SubjectForm;
import um.fds.agl.ter22.repositories.UserTERRepository;
import um.fds.agl.ter22.services.SubjectService;
import um.fds.agl.ter22.services.TeacherService;

@Controller
public class SubjectController implements ErrorController {

    @Autowired
    private SubjectService subjectService;
    @Autowired
    private TeacherService teacherService;
    @Autowired
    private UserTERRepository userTERRepository;

    @GetMapping("/listSubjects")
    public Iterable<SubjectTER> getSubjects(Model model) {
        Iterable<SubjectTER> subjects=subjectService.getSubjects();
        model.addAttribute("subjects", subjects);
        return subjects;
    }

    @ModelAttribute("allTeachers")
    public Iterable<Teacher> populateTeachers(){
        return teacherService.getTeachers();
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or hasRole('ROLE_TEACHER')")
    @GetMapping(value = { "/addSubject" })
    public String showAddSubjectPage(Model model) {

        SubjectForm subjectForm = new SubjectForm();
        model.addAttribute("subjectForm", subjectForm);

        return "addSubject";
    }

    @PostMapping(value = { "/addSubject"})
    public String addSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        SubjectTER subject;
        // subject not existing : create
        UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        subject=new SubjectTER(subjectForm.getSubjectName(), subjectForm.getSubjectDescription(), userTERRepository.findByLastName(userDetails.getUsername()), subjectForm.getSecondaryInCharge());
        subjectService.saveSubject(subject);
        return "redirect:/listSubjects";
    }

    @RequestMapping(value = "/addSubject", params = {"addSecondary"})
    public String addSecondary(final SubjectForm subjectForm){
        subjectForm.getSecondaryInCharge().add(new Teacher());
        return "addSubject";
    }

    @RequestMapping(value = "/addSubject", params = {"removeSecondary"})
    public String removeSecondary(final SubjectForm subjectForm, final HttpServletRequest req){
        final Integer secondaryId = Integer.valueOf(req.getParameter("removeSecondary"));
        subjectForm.getSecondaryInCharge().remove(secondaryId.intValue());
        return "addSubject";
    }

    @PostMapping(value = { "/updateSubject"})
    public String updateSubject(Model model, @ModelAttribute("SubjectForm") SubjectForm subjectForm) {
        SubjectTER subject;
        // subject already existing : update
        subject = subjectService.findById(subjectForm.getId()).get();
        subject.setSubjectName(subjectForm.getSubjectName());
        subject.setSubjectDescription(subjectForm.getSubjectDescription());
        subject.setSecondaryInCharge(subjectForm.getSecondaryInCharge());
        subjectService.saveSubject(subject);
        return "redirect:/listSubjects";
    }

    @RequestMapping(value = "/updateSubject", params = {"addSecondaryUpdate"})
    public String addSecondaryUpdate(final SubjectForm subjectForm){
        subjectForm.getSecondaryInCharge().add(new Teacher());
        return "updateSubject";
    }

    @RequestMapping(value = "/updateSubject", params = {"removeSecondaryUpdate"})
    public String removeSecondaryUpdate(final SubjectForm subjectForm, final HttpServletRequest req){
        final Integer secondaryId = Integer.valueOf(req.getParameter("removeSecondaryUpdate"));
        subjectForm.getSecondaryInCharge().remove(secondaryId.intValue());
        return "updateSubject";
    }

    @PreAuthorize("hasRole('ROLE_MANAGER') or (@subjectService.findById(#id).get()?.userInCharge?.lastName == authentication?.name)")
    @GetMapping(value = {"/updateSubject/{id}"})
    public String showSubjectUpdateForm(Model model, @PathVariable(value = "id") long id){
        SubjectTER subjectTER = subjectService.findById(id).get();
        SubjectForm subjectForm = new SubjectForm(id, subjectTER.getSubjectName(), subjectTER.getSubjectDescription(), subjectTER.getUserInCharge(), subjectTER.getSecondaryInCharge());
        model.addAttribute("subjectForm", subjectForm);
        return "updateSubject";
    }

    @GetMapping(value = {"/deleteSubject/{id}"})
    public String deleteSubject(Model model, @PathVariable(value = "id") long id){
        subjectService.deleteSubject(id);
        return "redirect:/listSubjects";
    }

}
