package um.fds.agl.ter22.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import um.fds.agl.ter22.entities.SubjectTER;
import um.fds.agl.ter22.repositories.SubjectRepository;

import java.util.Optional;


@Service
public class SubjectService {

    @Autowired
    private SubjectRepository subjectRepository;

    public Optional<SubjectTER> getSubject(final Long id) {
        return subjectRepository.findById(id);
    }

    public Iterable<SubjectTER> getSubjects() {
        return subjectRepository.findAll();
    }

    public void deleteSubject(final Long id) {
        subjectRepository.deleteById(id);
    }

    public SubjectTER saveSubject(SubjectTER subject) {
        SubjectTER savedSubject = subjectRepository.save(subject);
        return savedSubject;
    }

    public Optional<SubjectTER> findById(long id) {
        return subjectRepository.findById(id);
    }
}